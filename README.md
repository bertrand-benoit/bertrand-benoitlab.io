# Official Bertrand BENOIT resume page


## Used Technologies
 - [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) feature
 - [Jekyll](https://jekyllrb.com/): blog-aware, static site generator in Ruby


## Thanks
 - James Grant (aka sproogen) for his [Modern Resume Theme](https://github.com/sproogen/resume-theme), and his quick reactivity on issue and feature requests
 - Author of [Hacker Theme](https://github.com/pages-themes/hacker) which gave inspiration to my dark theme
 - Ademilter for his nice [404 page](/doesNotExist)


## Contact
Don't hesitate to [contact me](mailto:contact@bertrand-benoit.net) if you find something wrong.
You can [report issues](https://gitlab.com/bertrand-benoit/bertrand-benoit.gitlab.io/issues) and propose [merge requests](https://gitlab.com/bertrand-benoit/bertrand-benoit.gitlab.io/merge_requests).
